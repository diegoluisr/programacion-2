package co.edu.uniquindio.prog2.musicplayer.forms;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class LoginForm extends ScrollPane {

	private VBox mainVBox;
	
	private HBox usernameHBox;
	private Label usernameLabel;
	private TextField usernameField;

	private HBox passwordHBox;
	private Label passwordLabel;
	private PasswordField passwordField;
	
	private HBox loginbuttonHBox;
	private Button cancelButton;
	private Button loginButton;
	
	private HBox forgetHBox;
	private Label forgetLabel;
	private Hyperlink forgetLink;
	
	private Separator separator0;
	
	private VBox alterloginVBox;
	private Button facebookButton;
	private Button googleButton;
	private Button appleidButton;
	
	private Separator separator1;
	
	private HBox registerHBox;
	private Button registerButton;
	
	public LoginForm() {
		super();
		this.createInterface();
	}

	public LoginForm(Node arg0) {
		super(arg0);
		this.createInterface();
	}
	
	private void createInterface() {
		// INIT LOGIN FORM --------------------------------
		usernameLabel = new Label("Username");
		usernameLabel.setPrefWidth(160.0);
		usernameField = new TextField();
		usernameField.setPrefWidth(180.0);
		usernameHBox = new HBox();
		usernameHBox.setAlignment(Pos.CENTER_LEFT);
		usernameHBox.setSpacing(10.0);
		usernameHBox.getChildren().add(usernameLabel);
		usernameHBox.getChildren().add(usernameField);

		passwordLabel = new Label("Password");
		passwordLabel.setPrefWidth(160.0);
		passwordField = new PasswordField();
		passwordField.setPrefWidth(180.0);
		passwordHBox = new HBox();
		passwordHBox.setAlignment(Pos.CENTER_LEFT);
		passwordHBox.setSpacing(10.0);
		passwordHBox.getChildren().add(passwordLabel);
		passwordHBox.getChildren().add(passwordField);
		
		cancelButton = new Button("Cancel");
		loginButton = new Button("Login");
		loginbuttonHBox = new HBox();
		loginbuttonHBox.setAlignment(Pos.CENTER_RIGHT);
		loginbuttonHBox.setSpacing(10.0);
		loginbuttonHBox.getChildren().add(cancelButton);
		loginbuttonHBox.getChildren().add(loginButton);
		
		forgetLabel = new Label("Do you forget your password");
		forgetLink = new Hyperlink("Click  here!");
		forgetHBox = new HBox();
		forgetHBox.setAlignment(Pos.CENTER_LEFT);
		forgetHBox.getChildren().add(forgetLabel);
		forgetHBox.getChildren().add(forgetLink);
		
		// END LOGIN FORM ---------------------------------
		
		separator0 = new Separator();
		
		// INIT LOGIN WITH PLATFORMS ----------------------
		
		facebookButton = new Button("Login with Facebook");
		googleButton = new Button("Login with Google");
		appleidButton = new Button("Login with AppleID");
		alterloginVBox = new VBox();
		alterloginVBox.setAlignment(Pos.CENTER);
		alterloginVBox.setSpacing(10.0);
		alterloginVBox.getChildren().add(facebookButton);
		alterloginVBox.getChildren().add(googleButton);
		alterloginVBox.getChildren().add(appleidButton);
		
		// END LOGIN WITH PLATFORMS -----------------------
		
		separator1 = new Separator();
		
		// INIT REGISTER  ---------------------------------
		
		registerButton = new Button("Register");
		registerHBox = new HBox();
		registerHBox.setAlignment(Pos.CENTER);
		registerHBox.setSpacing(10.0);
		registerHBox.getChildren().add(registerButton);
		
		// END REGISTER -----------------------------------
		
		mainVBox = new VBox();
		mainVBox.setAlignment(Pos.CENTER);
		mainVBox.getChildren().add(usernameHBox);
		mainVBox.getChildren().add(passwordHBox);
		mainVBox.getChildren().add(loginbuttonHBox);
		mainVBox.getChildren().add(forgetHBox);
		
		mainVBox.getChildren().add(separator0);
		
		mainVBox.getChildren().add(alterloginVBox);
		
		mainVBox.getChildren().add(separator1);
		
		mainVBox.getChildren().add(registerHBox);
		
		for (Node node : mainVBox.getChildren()) {
			VBox.setMargin(node, new Insets(10.0));
		}
		
		this.setContent(mainVBox);
		this.prefHeight(400.0);
		this.prefWidth(400.0);
	}

}
