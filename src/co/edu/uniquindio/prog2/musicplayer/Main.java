package co.edu.uniquindio.prog2.musicplayer;
	
import java.io.IOException;

import co.edu.uniquindio.prog2.musicplayer.forms.LoginForm;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;


public class Main extends Application {
	@Override
	public void start(Stage stage) {
		
		// Use esta bandera para activar la version FXML o la version programatica.
		boolean flag = false;
		
		if (flag) {
			FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("/fxml/LoginForm.fxml"));
	        Scene scene;
			try {
				scene = new Scene(fxmlLoader.load(), 400, 400);
				stage.setTitle("Music Player - Login");
		        stage.setScene(scene);
		        stage.show();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else {
			try {
				LoginForm root = new LoginForm();
				Scene scene = new Scene(root ,400, 400);
				stage.setTitle("Music Player - Login");
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				stage.setScene(scene);
				stage.show();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
