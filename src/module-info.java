module MusicPlayer {
	requires javafx.controls;
	requires javafx.fxml;
	
	opens co.edu.uniquindio.prog2.musicplayer to javafx.graphics, javafx.fxml;
}
